Pareto Server
				A simple Http Server in Java

Capabilities:
	- handles GET requests over Http 1.0 or Http1.1
	- sends folder content or file for requested resource specified as URI in request
	- keep alive behaviour
	- serve multiple clients at the same time
	- multiplexed IO read
	- can be configured with a json config file

Prerequisites:
	- unix operating system (there is a crash on windows, tbd)
	- Java 8

Config:
	- create a config file or use the template one as first param at main
	- in config file, specify the root folder that the server will operate on
	(e.g {"server_content_location": "./tests/"})
	- the rest of the settings have defaults, but "server_content_location" is MANDATORY

Run:
	- create build or use the one in repo and specify the cofig file as first param at run
	e.g: java -jar ParetoServer.jar ./config.jar

Request examples:
	
	supposing the server serves from the folder "Test" with the following content:
	Test:--file.1
		 --file.2
		 ----dir1:   --file.3
		 		     --file.4
		 ----dir2:

	Request: GET / HTTP/1.1
	Response: a html containing a list with files in "Test"

	Request: GET /dir1/file.3
	Response: the content of file.3