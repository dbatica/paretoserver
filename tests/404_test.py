import socket
import sys
import time

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server_address = ('localhost', 8020)
print >>sys.stderr, 'connecting to %s port %s' % server_address
sock.connect(server_address)


sock.send("GET /random HTTP/1\r\n" +
"Host: localhost:8020\r\n" +
"User-Agent: curl/7.43.0\r\n" +
"Accept: */*\r\n\r\n")

# curl -Iv localhost:8020/dasdas 2>&1 | grep -i 'connection #0'
