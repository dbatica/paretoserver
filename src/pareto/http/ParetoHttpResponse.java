package pareto.http;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import pareto.ParetoServerConfig;
import pareto.util.TimeUtil;

public class ParetoHttpResponse {

	private int mnStatus = 0;
	private String mReason = null;
	private HashMap<String, String> mHeadersMap = null;
	// private ParetoHttp.eHttpVersion mVersion =
	// ParetoHttp.eHttpVersion.HTTP_11;

	private long mlContentLength;

	public ParetoHttpResponse() {
		mHeadersMap = new HashMap<String, String>();
		// default values for all
		this.addHeader("DATE", TimeUtil.getCurrentTime());
		this.addHeader("Server", ParetoServerConfig.getSharedInstance().getServerName());
		mlContentLength = 0;
	}

	protected int getStatus() {
		return mnStatus;
	}

	protected void setStatus(int status) {
		this.setStatus(status, null);
	}

	protected void setStatus(int status, String reason) {
		mnStatus = status;
		switch (status) {
		case 200:
			mReason = "OK";
			break;
		case 400:
			mReason = "Bad request";
			break;
		case 404:
			mReason = "Not Found";
			break;
		case 405:
			mReason = "Method Not Allowed";
			break;
		case 406:
			mReason = "Not Acceptable";
			break;
		case 408:
			mReason = "Request Timeout";
			break;
		case 413:
			mReason = "Payload Too Large";
			break;
		case 414:
			mReason = "URI Too Long";
			break;
		case 418:
			mReason = "Request too large";
			break;
		case 500:
			mReason = "Internal server error";
			break;
		case 501:
			mReason = "Not Implemented";
			break;
		case 505:
			mReason = "HTTP Version Not Supported";
			break;
		}

		if (null != reason) {
			mReason = reason;
		}

		if (status != 200) {
			this.addHeader("Connection", "Closed");
		}
	}

	protected void addHeader(String key, String value) {
		if (null != key && null != value) {
			mHeadersMap.put(key, value);
		}
	}

	protected void addContentLength(long totalLength) {
		this.addHeader("Content-Length", "" + totalLength);
		mlContentLength = totalLength;
	}
	
	protected boolean hasContentLength () {
		return mlContentLength > 0;
	}

	public boolean getKeepAlive() {
		if (null == mHeadersMap) {
			return false;
		}

		if (null != mHeadersMap.get("Connection")) {
			String value = mHeadersMap.get("Connection");
			if (0 == value.compareTo("keep-alive")) {
				return true;
			}
		}
		return false;
	}

	protected void addKeepAlive() {
		mHeadersMap.put("Connection", "keep-alive");
	}

	protected void addDoNotKeepAlive() {
		mHeadersMap.put("Connection", "closed");
	}

	protected byte[] getRawHeader() {

		String stringHeader = this.headerToString();
		byte[] rawHeader = stringHeader.getBytes(Charset.forName("UTF-8"));
		return rawHeader;
	}

	public String headerToString() {
		StringBuilder responseHeader = new StringBuilder();
		String firstLine = "HTTP/1.1 " + mnStatus + " " + mReason;
		responseHeader.append(firstLine + "\r\n");

		Iterator<Entry<String, String>> it = mHeadersMap.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, String> element = (Entry<String, String>) it.next();
			responseHeader.append(element.getKey() + ": " + element.getValue() + "\r\n");
		}
		responseHeader.append("\r\n");
		return responseHeader.toString();
	}

	public void release() {
		mReason = null;
		mHeadersMap.clear();
		mHeadersMap = null;
	}

}
