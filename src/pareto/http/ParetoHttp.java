package pareto.http;

public class ParetoHttp {

	public static enum eHttpVersion {
		UNKNOWN,
		HTTP_1,
		HTTP_11
	}
	
	public static enum eHttpMethod {
		UNKNOWN,
		GET,
		POST,
		DELETE,
		UPDATE,
		HEAD
	}
	
	
}
