package pareto.http;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * NOT USED
 * @author Daniel
 *
 */
public class FutureHttpParetoResponse implements Future<ParetoHttpResponse> {

	private final ParetoHttpResponse response;
	
	public FutureHttpParetoResponse(final ParetoHttpResponse rsp) {
		response = rsp;
	}

	@Override
	public boolean isDone() {
		if (null != response && response.getStatus() > 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isCancelled() {
		return false;
	}

	@Override
	public ParetoHttpResponse get(long timeout, TimeUnit unit)
			throws InterruptedException, ExecutionException, TimeoutException {
		if (null != response && response.getStatus() > 0) {
			return response;
		}
		return null;
	}

	@Override
	public ParetoHttpResponse get() throws InterruptedException, ExecutionException {
		if (null != response && response.getStatus() > 0) {
			return response;
		}
		return null;
	}

	@Override
	public boolean cancel(boolean mayInterruptIfRunning) {
		return false;
	}

}
