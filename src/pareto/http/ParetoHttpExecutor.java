package pareto.http;

import pareto.ParetoServerConfig;
import pareto.html.HtmlEncapsulator;
import pareto.http.ParetoHttp.eHttpMethod;
import pareto.http.ParetoHttp.eHttpVersion;
import pareto.http.ParetoHttpRequest.eParseResult;
import pareto.io.FileRequestManager;
import pareto.io.FileRequestManager.FileRequestManagerDelegate;
import pareto.io.FileRequestManager.eFileOperationResult;
import pareto.io.RawDataInputStream;
import pareto.io.RawDataInputStream.RawDataInputStreamDelegate;
import pareto.io.RawDataOutputStream;

import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Class that reads bytes from client socket input stream, parses them into a HttpRequest,
 * requests file content from FileManager, then sends response
 * The response in sent in 2 phases: first the http header and then the body
 * 
 * Supported methods: only GET (returns files and dir content)
 * Multiple requests are supported
 * 
 * An object is allocated for each connection
 * @author Daniel
 */
public class ParetoHttpExecutor implements RawDataInputStreamDelegate, FileRequestManagerDelegate {


	private FileRequestManager mFileRequestManager = null;
	private ParetoHttpRequest mRequest = null;
	private ParetoHttpResponse mResponse = null;

	private RawDataOutputStream mClientOutputStream;

	private boolean mbWaitForFileManagerResponse;//used to block waitToWriteResponseComplteted call if data is expected from FileRequestManager

	public ParetoHttpExecutor() {
		mFileRequestManager = FileRequestManager.getSharedIntance();
	}

	/**
	 * Starts reading the raw request, data is recived in "onDataReceived"
	 * on the same thread
	 * @param inputStream
	 */
	public void readRequestFromInputStream(RawDataInputStream inputStream) {
		mRequest = new ParetoHttpRequest();
		mResponse = new ParetoHttpResponse();
		mbWaitForFileManagerResponse = false;
		inputStream.readRawRequest(this, ParetoServerConfig.getSharedInstance().getRequestTimeout());
	}

	public void writeResponseToOutputStream(RawDataOutputStream outputStream) {
		mClientOutputStream = outputStream;
	}

	public void waitToWriteResponseComplteted() throws InterruptedException {
		if (mbWaitForFileManagerResponse) {
			synchronized (mResponse) {
				mResponse.wait();
			}
		}
		mRequest.release();
	}

	public boolean isKeepAliveConnection() {
		return mResponse.getKeepAlive();
	}

	// adds keep-alive or not
	private void addConnectionHeaderInResponse() {
		boolean addKeepAlive = false;
		if (mRequest.getVersion() == eHttpVersion.HTTP_11) {
			addKeepAlive = true;
		} else if (mRequest.getKeepAlive()) {
			addKeepAlive = true;
		}

		if (addKeepAlive) {
			mResponse.addKeepAlive();
		} else {
			mResponse.addDoNotKeepAlive();
		}
	}

	private void setResponseStatusAndSendToClient(int status) {
		this.setResponseStatusAndSendToClient(status, null);
	}

	private void setResponseStatusAndSendToClient(int status, String message) {
		mResponse.setStatus(status, message);
		System.out.println("Sending: \r\n" + mResponse.headerToString());
		mClientOutputStream.writeRawData(mResponse.getRawHeader());
	}

	// RawDataInputStreamDelegate implementation, data received from request
	// input stream
	@Override
	public boolean onDataReceived(byte[] data, boolean hasMore, Error err) {

		try {
			if (null != data && null == err) {

				eParseResult parseResult = mRequest.addBytesToRawRequestAndParse(data);
				if (eParseResult.STILL_PARSING_FIRST_LINE == parseResult
						|| eParseResult.STILL_PARSING_HEADER == parseResult) {
					return true;
				}
				
				System.out.println("Request: \r\n" + mRequest.toString());
				//finished parsing the request
				if (eParseResult.OK != parseResult) {
					setResponseStatusAndSendToClient(400, parseResult.toString());
					return false;
				}
				
				
				// it's ok
				if (false == hasMore) {

					if (mFileRequestManager.isRunning()) {
						this.addConnectionHeaderInResponse();
						String strUri = mRequest.getRequestURI();
						eHttpMethod method = mRequest.getMethod();
						
						eFileOperationResult addResult = mFileRequestManager.addNewTask(strUri, method, this);
						if (eFileOperationResult.FILE_NOT_FOUND == addResult) {
							setResponseStatusAndSendToClient(404);
							return false;
						}
						mbWaitForFileManagerResponse = true;
						
					} else {
						setResponseStatusAndSendToClient(500, "No files on server");
					}

					return false;
				}

			} else {

				if (null != err && err.getCause() instanceof RawDataInputStream.TimeoutException) {
					setResponseStatusAndSendToClient(408);
				}
				return false;
			}
		} catch (IOException e) {
			setResponseStatusAndSendToClient(500);
			return false;
		}
		return true;
	}

	// FileRequestManagerDelegate implementation
	@Override
	public boolean onFileRead(byte[] content, long offset, long totalLength, eFileOperationResult result) {


		if (eFileOperationResult.ERROR_AT_READ == result) {
			setResponseStatusAndSendToClient(500, "ERROR AT read file");
			return false;
		}

		if (0 == offset) {// first chunk
			mResponse.addContentLength(totalLength);
			setResponseStatusAndSendToClient(200);
		}

		boolean keepReading = false;
		if (null != content) {
			keepReading = mClientOutputStream.writeRawData(content);
		}
		long bytesSent = offset + content.length;
		if (bytesSent == totalLength || !keepReading) {
			synchronized (mResponse) {
				mResponse.notify();
			}
		}
		content = null;
		return keepReading;
	}
	
	@Override
	public void onDirectoryListed(String content, eFileOperationResult result) {
		if (eFileOperationResult.OK == result) {
			String strHtml = HtmlEncapsulator.encapsulateFileListInHtlm(content);
			byte[] htmlByte = strHtml.getBytes(Charset.forName("UTF-8"));
			mResponse.addContentLength(htmlByte.length);
			setResponseStatusAndSendToClient(200);
			mClientOutputStream.writeRawData(htmlByte);
			
			synchronized (mResponse) {
				mResponse.notify();
			}
			strHtml = null;
			content = null;
			htmlByte = null;
		} else {
			setResponseStatusAndSendToClient(500, "ERROR AT read file");
		}
	}
}
