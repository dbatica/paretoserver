package pareto.http;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;

import pareto.ParetoServerConfig;


public class ParetoHttpRequest {

	public static enum eParseResult {
		OK,
		STILL_PARSING_FIRST_LINE,
		STILL_PARSING_HEADER,
		INVALID_HEADER,
		INVALID_HEADER_VALUE,
		INVALID_FIRST_LINE,
		INVALID_PROTOCOL_VERSION,
		INVALID_HTTP_METHOD,
		URL_DECODE_ERROR,
		URI_TOO_LONG,
		REQUEST_TOO_LARGE
	}

	private String headers;

	private ByteArrayOutputStream mHeaderRawContentArray;
	private ByteArrayOutputStream mbodyRawContentArray;
	private boolean mbReachedEndOfHeader;
	
	private boolean mbParsedFirstLine;
	

	private ParetoHttp.eHttpMethod mMethod = ParetoHttp.eHttpMethod.UNKNOWN;
	private ParetoHttp.eHttpVersion mVersion = ParetoHttp.eHttpVersion.UNKNOWN;
	private String mRequestURI = null;
	private HashMap<String, String> mHttpHeadersMap = null;

	public ParetoHttpRequest() {

		mHeaderRawContentArray = new ByteArrayOutputStream();
		mbodyRawContentArray = new ByteArrayOutputStream();
		mbReachedEndOfHeader = false;
		mbParsedFirstLine = false;
	}

	protected int getRawHeaderSize () {
		return mHeaderRawContentArray.size();
	}
	
	// public getters
	public String getHeaders() {
		return headers;
	}

	public String getRequestURI() {
		return this.mRequestURI;
	}

	public ParetoHttp.eHttpMethod getMethod() {
		return this.mMethod;
	}

	public ParetoHttp.eHttpVersion getVersion() {
		return this.mVersion;
	}

	public boolean getKeepAlive() {
		if (null == mHttpHeadersMap) {
			return false;
		}

		if (null != mHttpHeadersMap.get("Connection")) {
			String value = mHttpHeadersMap.get("Connection");
			if (0 == value.compareTo("keep-alive")) {
				return true;
			}
		}
		return false;
	}

	// force GC to clean
	public void release() {
		mHeaderRawContentArray = null; 
		mbodyRawContentArray = null;
		headers = null;
		mRequestURI = null;
		if (null != mHttpHeadersMap) {
			mHttpHeadersMap.clear();
			mHttpHeadersMap = null;
		}
	}

	// return true if request it's ok, false otherwise
	protected eParseResult addBytesToRawRequestAndParse(byte[] content) throws IOException {

		long actualRequestSize = 0;
		if (null != mHeaderRawContentArray) {
			actualRequestSize += mHeaderRawContentArray.size();
		}
		
		if (null != mbodyRawContentArray) {
			actualRequestSize += mbodyRawContentArray.size();
		}
		
		long requestMaxLength = ParetoServerConfig.getSharedInstance().getRequestMaxLength();
		if ((actualRequestSize + content.length) > requestMaxLength) {
			return eParseResult.REQUEST_TOO_LARGE;
		}
		
		if (!mbReachedEndOfHeader) {
			int idx = getIndexForEndOfHeaderInRawContent(content);
			if (idx > 0) {
				mbReachedEndOfHeader = true;
				mHeaderRawContentArray.write(content, 0, idx);// can begin
																// parse-ing the
				mHeaderRawContentArray.flush();
				
				// add starting of the body
				int offset = idx + 4;
				int length = content.length - offset;
				if (length > 0) {
					mbodyRawContentArray.write(content, offset, length);
				}
				// parse header
				eParseResult requestParseResult = parseRawRequest();
				return requestParseResult;
			} else {
				mHeaderRawContentArray.write(content);
				mHeaderRawContentArray.flush();
				
				if (!mbParsedFirstLine) {
					byte[] rawContentArray = mHeaderRawContentArray.toByteArray();
					int firstLineIdx = getIndexForEndOfLineInContent(rawContentArray);
					if (firstLineIdx > 0) {
						mbParsedFirstLine = true;
						eParseResult firstLineParseResult = this.parseMethodAndVersion(new String(rawContentArray, 0, firstLineIdx)); 
						if (eParseResult.OK != firstLineParseResult) {
							mHeaderRawContentArray = null;
							return firstLineParseResult;
						}
					} else {
						
					}
				}
				return eParseResult.STILL_PARSING_HEADER;
			}
		} else {
			mbodyRawContentArray.write(content);
		}

		return eParseResult.OK;
	}
	
	private int getIndexForEndOfLineInContent(byte[] conent) {
		for (int i = 0; i < conent.length; ++i) {
			if ('\r' == (char) conent[i] && '\n' == (char) conent[i + 1]) {
				return i;
			}
		}
		return -1;
	}

	// return negative if not found, else the index of the end
	private int getIndexForEndOfHeaderInRawContent(byte[] conent) {
		// search for http headers empty line
		for (int i = 0; i < conent.length; ++i) {
			if ('\r' == (char) conent[i] && '\n' == (char) conent[i + 1] && '\r' == (char) conent[i + 2]
					&& '\n' == (char) conent[i + 3]) {
				return i;
			}
		}
		return -1;
	}

	// parse methods
	private eParseResult parseRawRequest() {
		headers = new String(mHeaderRawContentArray.toByteArray());

		// get method, resource & version from fisrs line
		String[] headerLines = headers.split("\r\n");
		if (headerLines.length > 0) {
			eParseResult firstLineParseResult = this.parseMethodAndVersion(headerLines[0]);
			if (eParseResult.OK != firstLineParseResult) {
				return firstLineParseResult;
			}
			
			mHttpHeadersMap = new HashMap<String, String>();
			// get the rest of the headers
			eParseResult headerParseResult = eParseResult.OK;
			for (int i = 1; i < headerLines.length; ++i) {
				headerParseResult = this.parseHeaderLine(headerLines[i]);
				if (eParseResult.OK != headerParseResult) {
					return headerParseResult;
				}
			}
			return eParseResult.OK;
		}
		return eParseResult.INVALID_HEADER;
	}

	private eParseResult parseMethodAndVersion(String methodAndVersionLine) {
		String[] params = methodAndVersionLine.split(" ");
		if (params.length < 3) {
			// invalid request
			return eParseResult.INVALID_FIRST_LINE;
		}

		switch (params[0]) {
		case "GET":
			mMethod = ParetoHttp.eHttpMethod.GET;
			break;
		case "POST":
			mMethod = ParetoHttp.eHttpMethod.POST;
			break;
		case "DELETE":
			mMethod = ParetoHttp.eHttpMethod.DELETE;
			break;
		case "UPDATE":
			mMethod = ParetoHttp.eHttpMethod.UPDATE;
			break;
		case "HEAD":
			mMethod = ParetoHttp.eHttpMethod.HEAD;
			break;
		default:
			mMethod = ParetoHttp.eHttpMethod.UNKNOWN;
			return eParseResult.INVALID_HTTP_METHOD;
		}

		try {
			String decodedUrl = URLDecoder.decode(params[1],"UTF-8");
			mRequestURI = "." + decodedUrl;// hack here
			if (mRequestURI.length() > ParetoServerConfig.getSharedInstance().getUriMaxLength()) {
				return eParseResult.URI_TOO_LONG;
			}
		} catch (UnsupportedEncodingException e) {
			return eParseResult.URL_DECODE_ERROR;
		}
		

		switch (params[2]) {
		case "HTTP/1":
			mVersion = ParetoHttp.eHttpVersion.HTTP_1;
			break;
		case "HTTP/1.1":
			mVersion = ParetoHttp.eHttpVersion.HTTP_11;
			break;
		default:
			mVersion = ParetoHttp.eHttpVersion.UNKNOWN;
			return eParseResult.INVALID_PROTOCOL_VERSION;
		}

		return eParseResult.OK;
	}

	private eParseResult parseHeaderLine(String line) {
		String[] params = line.split(": ");
		if (params.length == 2) {
			mHttpHeadersMap.put(params[0], params[1]);
			return eParseResult.OK;
		} else {
			return eParseResult.INVALID_HEADER_VALUE;
		}
	}
	
	public String toString () {
		String header = mHeaderRawContentArray.toString();
		return header;
	}
}
