package pareto.net;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import pareto.ParetoServerConfig;
import pareto.net.ParetoClientWorker;

/**
 * Shared instance, runs on main thread
 * Starts the server socket with settings specified in config file
 * Contains the thread pool executor with fixed size, specified in config file
 * Listens for incomming connections then passes them to a ClientWorker
 * 
 * @author Daniel
 */
public class ParetoConnectionsCoordinator {
	
	private ExecutorService mThreadPoolExecutor = null;  
	
	private static ParetoConnectionsCoordinator sShareInstance = null;
	private ParetoConnectionsCoordinator() {
		
	}
	
	public static ParetoConnectionsCoordinator getSharedInstance() {
		if(sShareInstance == null) {
			sShareInstance = new ParetoConnectionsCoordinator();
		}
		return sShareInstance;
	}
	
	private ServerSocket serverSocket;
	
	public boolean startListening(String strHost, int port) {
		try {
			serverSocket = new ServerSocket (port);
			System.out.println("Server listening on port: " + port);
			int numThreads = ParetoServerConfig.getSharedInstance().getNumWorkerThreads();
			if (numThreads > 0) {
				mThreadPoolExecutor = Executors.newFixedThreadPool(numThreads);//need to check this
				System.out.println("Worker threads: " + numThreads);
				this.run();
			} else {
				System.out.println("Could not start server: invalid num worker threads " + numThreads);
				return false;
			}

			//use main thread
						
		} catch (IOException e) {
			System.out.println("Could not start server: " + e.toString());
			return false;
		}
		return true;
	}
	
	public void run (){
		boolean bKeepListening = true;
		try {
			while (bKeepListening) {
				Socket clientSocket = serverSocket.accept();
				System.out.println("New Connection from: " + clientSocket.getInetAddress().getHostName());
				ParetoClientWorker worker = new ParetoClientWorker(clientSocket);
				mThreadPoolExecutor.submit(worker);
			}	
		}  catch (IOException e) {
			System.out.println("Could not start listen socket: " + e.toString());
			e.printStackTrace();
		} finally {
			try {
				serverSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
