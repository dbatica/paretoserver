package pareto.net;


import java.io.IOException;
import java.net.Socket;


import pareto.http.ParetoHttpExecutor;


import pareto.io.RawDataInputStream;
import pareto.io.RawDataOutputStream;

/**
 * Worker thread class that is responsible for connection with the client
 * This thread is managed by the thread pool defined in ParetoConnectionsCoordinator
 * This class does not know http protocol, it passes streams to a ParetoHttpExecutor that decides what to do
 * 
 * @author Daniel
 *
 */
public class ParetoClientWorker implements Runnable {

	private Socket mClientSocket;
	private RawDataInputStream mClientInputStream;
	private RawDataOutputStream mClientOutputStream;
	private ParetoHttpExecutor mHttpExecutor;


	public ParetoClientWorker(Socket clientSocket) {
		this.mClientSocket = clientSocket;
		this.mHttpExecutor = new ParetoHttpExecutor();
	}

	public void run() {

		try {
			
			mClientInputStream = new RawDataInputStream(mClientSocket.getInputStream());
			mClientOutputStream = new RawDataOutputStream(mClientSocket.getOutputStream());

			
			boolean keepAlive = true;
			
			while (keepAlive) {//todo: close connection after MAX_TIME_FOR_SESSION
			
				mHttpExecutor.writeResponseToOutputStream(mClientOutputStream);//just a setter here
				mHttpExecutor.readRequestFromInputStream(mClientInputStream);
				
				// wait here because of keep alive, could have split read and write into 2 different workers
				mHttpExecutor.waitToWriteResponseComplteted();
				
				keepAlive = mHttpExecutor.isKeepAliveConnection();
			}
			
		} catch (IOException e) {
			//TODO log error
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO log error
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				mClientInputStream.close();
				mClientOutputStream.close();
				mClientSocket.close();
				System.out.println("Closed a connection");
				this.release();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void release() {
		mClientSocket = null;
		mClientInputStream = null;
		mClientOutputStream = null;
		mHttpExecutor = null;
	}

	
}
