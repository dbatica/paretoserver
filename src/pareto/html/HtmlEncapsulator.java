package pareto.html;

import java.io.File;

import pareto.ParetoServerConfig;

public class HtmlEncapsulator {

	/**
	 * 
	 * @param files:
	 *            files paths separated by \r\n
	 * @return: html like string
	 */
	public static String encapsulateFileListInHtlm(String files) {
		String[] filePaths = files.split("\r\n");
		StringBuilder html = new StringBuilder();
		html.append("<html>");
		html.append(String.format("%s  <br /><br /><br />", ParetoServerConfig.getSharedInstance().getServerName()));

		for (String filePath : filePaths) {
			String fileName = getFileNameFromPath(filePath);
			String fileLine = String.format("<a href=\"./%s\">%s</a> <br />", filePath, fileName);
			html.append(fileLine);
		}

		html.append("</html>");
		String htmlString = html.toString();
		html = null;

		return htmlString;
	}

	private static String getFileNameFromPath(String filePath) {
		try {
			String[] components = filePath.split(File.separator);// crash here
																	// on
																	// windows
			if (null != components && components.length > 0) {
				return components[components.length - 1];
			}
		} catch (Exception e) {
			System.out.println("Error at parse dir: " + e.getMessage());
		}
		return null;
	}

}
