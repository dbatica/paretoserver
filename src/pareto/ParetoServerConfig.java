package pareto;

import org.json.*;
/**
 * Singletone used to load setting from json config file. 
 * org.json is used to parse the file, see https://github.com/stleary/JSON-java
 * 
 * config template:
 * {
    "server_name": String, name of the server, will be displayed in browser at list dir, has default
    "server_port": int, default is 8020,
    "max_concurent_sessions": int, number of concurrent worker threads used to establish connections, default = number of available procs
    "request_timeout": int, in miliseconds, time to wait for http request, measured from connection established or response send
    "max_time_for_session": int, in milliseconds, TODO: not yet used
    "uri_max_length": int, in bytes, default 2048
    "request_max_length": long, in bytes,
    "server_content_location": String, MANDATORY, no files will be served without it
	}
 * @author Daniel
 *
 */

public class ParetoServerConfig {

	//default values for config options
	private static final String SERVER_NAME = "Pareto 80/20";
	private static final int SERVER_PORT = 8020;
	private static final int REQUEST_TIMEOUT = 1000;// milisecond
	private static final int MAX_TIME_FOR_SESSION = 5000; // miliseconds
	private static final long REQUEST_MAX_LENGTH = 10000000;// bytes
	private static final int URI_MAX_LENGTH = 2048;// bytes

	private ParetoServerConfig() {

	}

	private static ParetoServerConfig sharedInstance;

	/**
	 * 
	 * @return Shared instance object
	 */
	public static ParetoServerConfig getSharedInstance() {
		if (null == sharedInstance) {
			sharedInstance = new ParetoServerConfig();

			// set default values
			sharedInstance.serverName = SERVER_NAME;
			sharedInstance.requestTimeout = REQUEST_TIMEOUT;
			sharedInstance.requestMaxLength = REQUEST_MAX_LENGTH;
			sharedInstance.numWorkerThreads = Runtime.getRuntime().availableProcessors();
			sharedInstance.uriMaxLength = URI_MAX_LENGTH;
			sharedInstance.serverPort = SERVER_PORT;
			sharedInstance.sessionMaxTime = MAX_TIME_FOR_SESSION;
		}
		return sharedInstance;
	}

	private String serverName = null;
	private int serverPort = 0;
	private int requestTimeout = -1;
	private int sessionMaxTime = -1;
	private long requestMaxLength = -1;
	private int uriMaxLength = -1;
	private int numWorkerThreads = 0;
	private String serverContentLocation = null;

	//getters
	public String getServerName() {
		return serverName;
	}

	public int getRequestTimeout() {
		return requestTimeout;
	}

	public int getSessionMaxTime() {//not used yet
		return sessionMaxTime;
	}

	public long getRequestMaxLength() {
		return requestMaxLength;
	}

	public int getNumWorkerThreads() {
		return numWorkerThreads;
	}

	public int getUriMaxLength() {
		return uriMaxLength;
	}

	public int getSeverPort() {
		return serverPort;
	}
	
	public String getServerContentLocation () {
		return serverContentLocation;
	}

	/**
	 * Loads values from the specified input file path and overwrites the default value
	 * Called in main at, before server starts up
	 * Uses  JSONObject to parse the settings
	 * @param configFile: path of the file
	 */
	public void loadSettingsFromConfigfile(String configFilePath) {
		JSONObject json = new JSONObject(configFilePath);

		serverName = json.optString("server_name", SERVER_NAME);
		serverPort = json.optInt("server_port", SERVER_PORT);
		requestTimeout = json.optInt("request_timeout", REQUEST_TIMEOUT);
		sessionMaxTime = json.optInt("max_time_for_session", MAX_TIME_FOR_SESSION);
		numWorkerThreads = json.optInt("max_concurent_sessions", Runtime.getRuntime().availableProcessors());
		uriMaxLength = json.optInt("uri_max_length", URI_MAX_LENGTH);
		requestMaxLength = json.optLong("request_max_length", REQUEST_MAX_LENGTH);
		serverContentLocation = json.optString("server_content_location", null);

		json = null;

	}
}
