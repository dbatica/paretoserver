package pareto.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtil {

	private static DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");// this leaks
	
	public static String getCurrentTime() {
		
		   //get current date time with Date()
		Date date = new Date();
		String strDateFormat = dateFormat.format(date); 
		date = null;
		return strDateFormat;
	}

}
