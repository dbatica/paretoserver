package pareto.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Comparator;

import pareto.http.ParetoHttp.eHttpMethod;
import pareto.io.FileRequestManager.FileRequestManagerDelegate;

/**
 * Class that defines objects consumed by FileRequestManager
 * @author Daniel
 *
 */
public class FileRequest {
	
	//NOT USED
	public static Comparator<FileRequest> lastTimeAccessComparator = new Comparator<FileRequest>(){
		
		@Override
		public int compare(FileRequest r1, FileRequest r2) {
            return (int) (r2.lastTimeAccesed - r1.lastTimeAccesed);
        }
	};
	
	protected static enum eFileOperation {
		READ, WRITE,
	}
	
	
	private FileRequestManagerDelegate delegate;
	private eFileOperation operation;
	private String resource;

	
	protected long lastTimeAccesed;//updated with current timestamp at each read
	// split into substasks
	protected long offset;
	protected long fileSize;
	protected boolean isDirectory;
	protected InputStream fileInputStream;
	protected long maxBytesToRead;
	

	public void relese() {
		delegate = null;
		resource = null;
		fileInputStream = null;
	}
	
	public FileRequestManagerDelegate getDelegate() {
		return this.delegate;
	}

	public eFileOperation getOperation() {
		return this.operation;
	}

	public String getResourceName() {
		return this.resource;
	}
	
	//used by file requestmanager ar first process
	protected boolean createInputStreamForResource () {
		File fileResource = new File(this.resource);
		this.fileSize = fileResource.length();
		this.fileInputStream = null;
		
		try {
			this.fileInputStream = new FileInputStream(fileResource);
			return true;
		} catch (FileNotFoundException e) {
			return false;
		}
	}

	public FileRequest(String strResource, eHttpMethod method, FileRequestManagerDelegate delegate) {
		this.fileSize = 0;
		this.isDirectory = false;
		this.lastTimeAccesed = 0;
		this.offset = 0;
		this.resource = strResource;
		this.delegate = delegate;

		if (eHttpMethod.GET == method) {
			operation = eFileOperation.READ;
		} else if (eHttpMethod.POST == method) {
			operation = eFileOperation.WRITE;
		}
	}
}