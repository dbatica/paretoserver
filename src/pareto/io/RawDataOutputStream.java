package pareto.io;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Helper wrapper over output stream
 * Used to trim bloated code from catching exceptions 
 * @author Daniel
 */
public class RawDataOutputStream {
	
	private OutputStream mOutputStream;
	
	public RawDataOutputStream(OutputStream stream) {
		mOutputStream = stream;
	}
	
	/**
	 * Writes data to outputstream specified in constructor
	 * @param rawData: data to write
	 * @return true if success, false otherwise
	 */
	public boolean writeRawData(byte[] rawData) {
		try {
			if (null != rawData && rawData.length > 0) {
				mOutputStream.write(rawData);
				mOutputStream.flush();
			}
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	public void close() {
		try {
			mOutputStream.close();
			mOutputStream = null;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
