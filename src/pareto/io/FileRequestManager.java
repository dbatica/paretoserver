package pareto.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.Queue;

import pareto.http.ParetoHttp.eHttpMethod;
import pareto.io.FileRequest.eFileOperation;
import pareto.io.RawDataInputStream.RawDataInputStreamDelegate;

/**
 * Singleton used to execute File reads request.
 * A dedicated thread is used to read (and write, tbd) files. requested files are 
 * enqueued in a last time access priority queue. Thread pops requests from queue and process them.
 * A file read request is split into multiple subtasks so other requests can be served also
 * 
 * Requests are submitted via FileRequestManager.addNewTask method, then processed
 * in a async manner, the result is return via a delegate that must implement "FileRequestManagerDelegate"
 * @author Daniel
 *
 */
public class FileRequestManager extends Thread {

	private static final int READ_BUFF_SIZE = 1000000;// 1mb
	private static final int MAX_SINGLE_READ_MULTIPLIER = 10;// 1mb

	public static enum eFileOperationResult {
		OK, FILE_NOT_FOUND, ERROR_AT_READ, ERROR_AT_WRITE,
	}

	/**
	 * Callback interface for read request
	 * Each request has a delegate object that implements this interface.
	 * 
	 */
	public interface FileRequestManagerDelegate {
		/** 
		 * @param content: array of bytes
		 * @param offset: the offset of the content in the file read
		 * @param totalLength: the size of the file
		 * @return: true if should read next chunk, false otherwise (e.g: called with false when request outputstream is closed)
		 */
		public boolean onFileRead(byte[] content, long offset, long totalLength, eFileOperationResult result);
		
		/**
		 * 
		 * @param content: string with filenames separated by \r\n
		 * @patam result: OK if success
		 */
		public void onDirectoryListed(String content, eFileOperationResult result);
	}

	

	private static FileRequestManager sharedInstace;
	private Queue<FileRequest> taskPool;
	private boolean keepRunning;

	private FileRequestManager() {

	}
	/**
	 * 
	 * @return the single instance of this class
	 */
	public static FileRequestManager getSharedIntance() {
		if (null == sharedInstace) {
			sharedInstace = new FileRequestManager();
		}

		return sharedInstace;
	}
	
	/**
	 * 
	 * @return true if thread is started (will not start if mount location does not exists)
	 */
	public boolean isRunning() {
		return this.isAlive();
	}

	private String strMountLocationPath;

	/**
	 * Sets the location from where to serve files
	 * @param contentLocation: 
	 * @return true if mount with success and thread started
	 */
	public boolean mountOnLocation(String contentLocation) {
		if (null == contentLocation) {
			return false;
		}
		boolean result = false;

		File directory = new File(contentLocation).getAbsoluteFile();

		if (directory.exists()) {
			try {
				String strCanonicalPath = directory.getCanonicalPath();
				result = (System.setProperty("user.dir", strCanonicalPath) != null);
				strMountLocationPath = strCanonicalPath + File.separator;
			} catch (IOException e) {
				return false;
			}
		}

		if (result) {
			taskPool = new LinkedList<FileRequest>();
//			taskPool = new PriorityQueue<FileRequest>(FileRequest.lastTimeAccessComparator);
			keepRunning = true;
			this.start();
		}

		return result;
	}

	/**
	 * Called from ParetoHttpExecutor objects when GET http requests are recived
	 * The result is returned via delegate specified
	 * 
	 * @param resource: path of the resource
	 * @param method: http method, only GET at this time
	 * @param delegate
	 * @return: if resource is not found FILE_NOT_FOUND is returned, OK otherwise
	 */
	public eFileOperationResult addNewTask(String resource, eHttpMethod method, FileRequestManagerDelegate delegate) {
		synchronized (taskPool) {
			String strResourcePath = strMountLocationPath + resource;
			Path resourcePath = Paths.get(strResourcePath);
			if (Files.exists(resourcePath, LinkOption.NOFOLLOW_LINKS)) {
				FileRequest request = new FileRequest(strResourcePath, method, delegate);
				if (Files.isDirectory(resourcePath)) {
					request.isDirectory = true;
				}

				taskPool.add(request);
				taskPool.notify();
				return eFileOperationResult.OK;
			}

			return eFileOperationResult.FILE_NOT_FOUND;
		}
	}
	
	private void addSubTask (FileRequest subtask) {
		synchronized (taskPool) {
			taskPool.add(subtask);
			taskPool.notify();
		}
	}

	private FileRequest getNextRequest() {
		synchronized (taskPool) {
			if (taskPool.size() > 0) {
				//get task with min lastTimeAccesed
				FileRequest requestToProcess = null;
				long minLastAccess = System.currentTimeMillis();
				for (FileRequest request : taskPool) {//O(n) could use faster 
					if (minLastAccess > request.lastTimeAccesed) {
						requestToProcess = request;
						minLastAccess = request.lastTimeAccesed;
					}
				}
				
				taskPool.remove(requestToProcess);
				return requestToProcess; 
			}
			return null;
		}
	}
	
	/**
	 * Called from main thread if server socket thread fails to bind
	 */
	public void stopThread() {
		keepRunning = false;
		this.interrupt();
	}

	// TODO write file from post
	public void run() {
		System.out.println("I/O thread running");

		try {
			while (keepRunning) {
				synchronized (taskPool) {
					taskPool.wait();
				}

				FileRequest request = null;
				while (null != (request = this.getNextRequest())) {

					if (eFileOperation.READ == request.getOperation()) {// read from disk
						if (request.isDirectory) {// send dir content
							readDirRequestAndSendToDelegate(request);
						} else {// send file in chunks
							readFileRequestAndSendToDelegate(request);
						}

					} else {// write to IO
						// request.getDelegate().onFileRead(null, 0);
					}
				}
			}
		} catch (InterruptedException e) {
			System.out.println("IO Thread stopped: " + e.getMessage());
			// todo, call all delegates in taskPool with error
		}
	}



	// could use a cache here (see MappedByteBuffer)
	private void readFileRequestAndSendToDelegate(FileRequest request) {

		FileRequestManagerDelegate delegate = request.getDelegate();
		if (0 == request.offset) {
			boolean hasInputStream = request.createInputStreamForResource();
			if (!hasInputStream) {
				delegate.onFileRead(null, 0, 0, eFileOperationResult.ERROR_AT_READ);
				return;
			}
		}
		
		long fileSize = request.fileSize;
		InputStream inputStream = request.fileInputStream;
		// maybe need to check if inputstream is still oppen
		
		int buffSize = (int) Math.min(request.fileSize - request.offset, READ_BUFF_SIZE);
		request.maxBytesToRead = (long) Math.min(request.fileSize - request.offset, buffSize * MAX_SINGLE_READ_MULTIPLIER);// send max 10 * READ_BUFF_SIZE  

		request.lastTimeAccesed = System.currentTimeMillis();
		RawDataInputStream readerInputStream = new RawDataInputStream(inputStream);
		readerInputStream.readRawRequest(new RawDataInputStreamDelegate() {

			@Override
			public boolean onDataReceived(byte[] data, boolean hasMore, Error err) {
				if (null == err) {

					boolean keepReading = delegate.onFileRead(data, request.offset, request.fileSize, eFileOperationResult.OK);
					if (keepReading) {
						request.offset += (long) data.length;
						request.maxBytesToRead -= (long)data.length;
						keepReading =  (request.maxBytesToRead > 0);
					} else {
						//received false from http executor, probably requestoutputstream is closed
						request.offset = request.fileSize;
					}

					data = null;
					return keepReading;
				} else {
					delegate.onFileRead(null, 0, fileSize, eFileOperationResult.ERROR_AT_READ);
					request.offset = request.fileSize;
					return true;
				}
			}
		}, 0, buffSize);//use no timeout, no need
		
		if (request.offset == request.fileSize) {
			request.relese();
			readerInputStream.close();
		} else {
			addSubTask(request);
		}

	}

	private void readDirRequestAndSendToDelegate(FileRequest request) {
		File fileResource = new File(request.getResourceName());
		String content = getDirectoryContent(fileResource);
		request.getDelegate().onDirectoryListed(content, eFileOperationResult.OK);
	}
	
	private String getDirectoryContent(File dir) {
		StringBuilder content = new StringBuilder();
		File[] files = dir.listFiles();
		
		for (File file : files) {
			String strFilePath = "." + File.separator;
			Path filePath;
			try {
				filePath = Paths.get(file.getCanonicalPath());
				String relativePath = filePath.toString().substring(strMountLocationPath.length());
				strFilePath += relativePath;
				content.append(strFilePath + "\r\n");
			} catch (IOException e) {
			}
		}
		return content.toString();
	}

}