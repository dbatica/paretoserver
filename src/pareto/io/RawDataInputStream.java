package pareto.io;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;


/**
 * Helper wrapper over input stream, data is read in chunks and return via a delegate 
 * that implements RawDataInputStreamDelegate, this is done on the same thread where the read was called.
 * 
 * Timeout and buffer size can be specified.
 * 
 * @author Daniel
 *
 */
public class RawDataInputStream {

	/**
	 * Callback interface for read request
	 * 
	 * @param data: data returned chunk, since data is sent serial, no need for specifying the offset
	 * @param hasMore: true if end of stream is reached, or no data available
	 * @param err: null or TimeoutException
	 * @return: true to keep reading next chunk, false othewise
	 * @author Daniel
	 *
	 */
	public interface RawDataInputStreamDelegate {
		public boolean onDataReceived(byte[] data, boolean hasMore, Error err);
	}

	public class TimeoutException extends Exception {

		/**
		 * Thrown when request timeout occurs
		 */
		private static final long serialVersionUID = 1166358184853911393L;

	}

	private InputStream mInputStream;

	public RawDataInputStream(InputStream inputStream) {
		this.mInputStream = inputStream;
	}

	private final static int IO_BUFF_SIZE = 2048;
	private int mnBuffSize = IO_BUFF_SIZE;

	/**
	 *  Reads from inputstream specified in constructor and returns data via delegate
	 *  
	 * @param delegate: the object that will receive the data
	 * @param requestTimeout: if > 0, the read will wait until available data, if no data is received, callback is called with TimeoutException err param
	 * @param buffSize: optional, can be increased if necessary
	 */
	public void readRawRequest(RawDataInputStreamDelegate delegate, int requestTimeout, int buffSize) {
		this.mnBuffSize = buffSize;
		this.readRawRequest(delegate, requestTimeout);
	}


	public void readRawRequest(RawDataInputStreamDelegate delegate, int requestTimeout) {

		byte[] buff = null;

		try {
			
			if (requestTimeout > 0) {
				// wait for request
				// could use some cool java neo async read
				long startTime = System.currentTimeMillis();
				long elapsedTime = 0;
				while ((mInputStream.available() <= 0) && (elapsedTime < requestTimeout)) {
					elapsedTime = System.currentTimeMillis() - startTime;
					Thread.sleep(10);// not using timers because of the overhead
				}

				if (mInputStream.available() <= 0) {
					delegate.onDataReceived(null, false, new Error(new TimeoutException()));
					return;
				}
			}

			buff = new byte[mnBuffSize];
			int bytesRead = -1;
			while (mInputStream.available() > 0) {

				bytesRead = mInputStream.read(buff, 0, mnBuffSize);
				if (bytesRead > 0) {

					byte[] copyArray = Arrays.copyOf(buff, bytesRead);
					boolean keepReading = delegate.onDataReceived(copyArray, (bytesRead == mnBuffSize), null);
					copyArray = null;
					if (false == keepReading) {
						break;
					}
				} else {
					delegate.onDataReceived(null, false, null);
					break;
				}
			}
			buff = null;
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		} finally {
			buff = null;
		}
	}

	public void close() {
		try {
			mInputStream.close();
			mInputStream = null;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
