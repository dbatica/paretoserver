import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import pareto.ParetoServerConfig;
import pareto.io.FileRequestManager;
import pareto.net.ParetoConnectionsCoordinator;

/**
 * File based web server. GET methods accepted, config file can be used to specify
 * options such as: max simultaneous connections, root folder of the accessible files, etc
 * 
 * Pareto is the 80/20 rule (https://en.wikipedia.org/wiki/Pareto_principle)
 * 
 */


/**
 * Entry point of the server
 * @author Daniel
 * 
 */
public class ParetoServer {

	/**
	 * 
	 * @param args[0]: specify the config file path @see ParetoServerConfig  
	 */
	public static void main(String[] args) {
		
		//read settings from config file
		try {
			if (args.length > 0) {
				String content = new String(Files.readAllBytes(Paths.get(args[0])));
				ParetoServerConfig.getSharedInstance().loadSettingsFromConfigfile(content);
				args = null;
				content = null;
			}
		} catch (IOException e) {
			System.out.println("Cloud not load config file, using the defaults");
		}
		
		// mount root public server dir and start the IO thread
		String contentLocation = ParetoServerConfig.getSharedInstance().getServerContentLocation();
		boolean contentIsAccessible = FileRequestManager.getSharedIntance().mountOnLocation(contentLocation);
		if (false == contentIsAccessible) {
			System.out.println("Could not load server content on location: " + contentLocation);
		} else {
			System.out.println("Serving from location: " + contentLocation);
		}
		contentLocation = null;
		
		//start server
		ParetoConnectionsCoordinator connectionCoordinator = ParetoConnectionsCoordinator.getSharedInstance();
		if(connectionCoordinator.startListening(null, ParetoServerConfig.getSharedInstance().getSeverPort())) {
			System.out.println("Server will close");
			
		} else {
			FileRequestManager.getSharedIntance().stopThread();
			System.out.println("Could not start server");
		}
		
	}

}
